---
title: "Round1 - Blumenau-SC"
date: 2020-01-19T19:55:15-03:00
draft: false
---

## Round 1 - Blumenau-SC/Brazil

### Mettings

- M1: Introdução ao Kernel do Linux
    - When: 21/May/2019
    - Presenters: David Emmerich Jourdain / Marcos Paulo de Souza
    - [slides](/slides/round1-bnu/lkcamp-M1.pdf)
    - [video](https://www.youtube.com/watch?v=Sjx2QO_t0EM)

- M2: Hello World! Creating your first device driver in Linux Kernel
    - When: 25/Jun/2019
    - Presenters: Marcos Paulo de Souza

