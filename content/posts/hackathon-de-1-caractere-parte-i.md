---
title: "Hackathon de 1 Caractere - Parte I"
date: 2022-11-10
draft: false
categories: ["Tutorial"]
tags: ["Hackathon", "Git", "SECOMP"]
authors: ["Ícaro Chiabai"]
---

Olá, nesse post esperamos te ajudar a concluir as etapas necessárias relacionadas ao _Git_ para que consiga participar tranquilamente do restante do Hackathon!

## O que é o _Git_?

O _Git_ é um Software Livre que atua como um Sistema de Controle de Versão, ou seja, é utilizado para realizar a manutenção da distribuição de diversas aplicações! Podemos usar o _Git_ para ir liberando _releases_ de nossa aplicação, anotações, livro, qualquer coisa contributiva, basta criarmos um repositório e disponibilizarmos em uma plataforma como o _GitHub_ ou o _GitLab_.

## Instalando o _Git_

Seguem abaixo algumas linhas de comando para a instalação do _Git_ em algumas distribuições de _Linux_.


```bash
sudo apt install git            # Debian/Ubuntu
sudo dnf install git-all        # Fedora
sudo pacman -Syu git            # Arch
```

## Criando sua conta!

### GitLab

Crie sua conta através deste [link](https://www.gitlab.com/users/sign_up/)

### GitHub

Crie sua conta através deste [link](https://www.github.com/signup)


## Sua primeira contribuição (no Hackathon)

### Realizando o _Fork_

{{< alerts warning
"Certifique-se de estar devidamente _logado_ para continuar com as etapas abaixo!"
>}}
#### GitLab

Vamos realizar o _fork_ do seguinte [repositório](https://gitlab.com/lkcamp/first-contribution).

No canto direito do título do repositório, haverá um botão escrito _Fork_, clique nele. Ele o redicionará para uma tela onde será possível a alteração de informações, mas nada será alterado. Selecione o seu usuário na sessão "_Select a namespace_" e o nível de visibilidade como "_public_". Prossiga clicando no botão _Fork Project_. Avance para a próxima sessão.

#### GitHub

Vamos realizar o _fork_ do seguinte [repositório](https://github.com/isabellabreder/lkcamp-first-contribution).

No canto direito do título do repositório, haverá um botão escrito "_Fork_", clique nele. Em seguida, selecione o botão "_Create Fork_". Avance para a próxima sessão.

### Clonando o repositório localmente

No seu repositório _forkado_, selecione o botão "_Clone_", isso abrirá um menu com a opção "_Clone with HTTPS_", copie o _link_ associado pois iremos utilizá-lo em breve.

Vamos criar um diretório local para armazenarmos o repositório remoto em nossas máquinas para podermos realizar alterações. Para isso, execute as seguintes linhas de comando.

```bash
mkdir -p ~/Projects
cd ~/Projects
git clone link first-contribution   # Substitua a palavra-chave link pelo seu próprio
cd first-contribution
```

Feito isso, podemos avançar para próxima sessão.

### Criando e acessando uma _branch_ em seu repositório local

Basta rodarmos a seguinte linha de comando.

```bash
git checkout -b add-nome-sobrenome          # A não ser que seu nome seja "nome" e sobrenome seja "sobrenome", altere esses valores!
```

### Configurando coisas importantes do Git

#### Global Config

Esse tipo de configuração é feito, normalmente, apenas uma vez, a não ser que você tenha alterado algum dado importante seu. Basta seguir rodando as linhas de comando seguintes.

```bash
git config --global user.name "Nome Sobrenome"
git config --global user.email "seu.email@email.org"
```

#### Personal Access Token (PAT)

Para permitimos o _push_ de _commits_, precisamos de um Token de Acesso! Para isso, podemos seguir criando eles em suas respectivas plataformas.

##### GitLab

Clique em seu usuário no _GitLab_, em seguida, selecione a opção "_Preferences_".

![](/imgs/posts/hackathon-de-1-caractere/gitlab-preferences.png)

Em seguida, no lado esquerdo, selecione a opção "_Access Tokens_".

![](/imgs/posts/hackathon-de-1-caractere/gitlab-selectpat.png)

Preencha o campo "_Token Name_" como quiser, recomendamos utilizar "Hackathon". Selecione apenas a caixa "_api_" e continue clicando no botão "_Create personal access token_". 

O valor do Token de Acesso deverá estar visível, como mostra a figura abaixo.

![](/imgs/posts/hackathon-de-1-caractere/gitlab-token.png)

##### GitHub

Clique em seu usuário no _GitHub_, em seguida, selecione a opção "_Settings_".

![](/imgs/posts/hackathon-de-1-caractere/github-settings.png)

No menu da esquerda, selecione a opção "_Developer Settings_", fica lá embaixo!

![](/imgs/posts/hackathon-de-1-caractere/github-devset.png)

Agora selecione a opção "_Tokens (classic)_". Em seguida, clique em "_Generate new token (classic)_".

![](/imgs/posts/hackathon-de-1-caractere/github-tokens.png)

Em seguida, adicione uma identificação e marque a caixa "_repo_". Finalize gerando o novo token.

![](/imgs/posts/hackathon-de-1-caractere/github-generatingtoken.png)

{{< alerts warning
"Certifique-se de salvar o Token gerado! Será importante para terminarmos o que começamos."
>}}

### Contribuindo!

Agora vamos, finalmente, contribuir para o _Fork_ realizado. Volte para o diretório que criamos anteriormente.

Edite o arquivo ___Contributors.md___, adicionando no meio do arquivo a linha:

> [Nome Sobrenome](link-do-seu-perfil)

Adicione as mudanças feitas para o _stage_ com a linha de comando

```bash
git add -A
git commit -m "Uma mensagem fofa."
git push
```

Quando solicitado sua autorização, siga os passos caso:

#### GitLab

> Username: oauth2
>
> Senha: token-de-acesso-criado

#### GitHub

> Username: seu-usuario
>
> Senha: token-de-acesso-criado

### Criando o Merge Request

Essa etapa será responsável por mesclar o repositório que você fez o _Fork_ com o seu repositório local.

Basta seguirmos os passos ilustrados abaixo.

![](/imgs/posts/hackathon-de-1-caractere/github-merge1.png)

Para GitLab:

![](/imgs/posts/hackathon-de-1-caractere/github-merge2.png)

Para GitHub:

![](/imgs/posts/hackathon-de-1-caractere/github-merge3.png)

Por enquanto é isso! Esperamos você depois do _Coffee break_!