---
title: "Calourada LKCAMP 2023"
date: 2023-02-27T12:25:20-03:00
draft: false
categories: ["Tutorial"]
tags: ["Debian", "Tradução"]
authors: ["Mariana Arias"]
---

Para acessar todos os links utilizados nesse blog e mais informações sobre o
LKCAMP, acesse o [link](https://lkcamp.gitlab.io/lynx/calourada/).


## O que é Debian?

Debian é um Sistema Operacional inteiramente de software livre e mantido pelo Projeto Debian, com a ajuda de contribuidores espalhados pelo mundo e sem fins lucrativos.


Existem muitas formas de contribuir para o Projeto Debian, como empacotamentos, traduções e pequenas modificações. Nesse blog, iremos mostrar como realizar uma contribuição de tradução para o Debian.

## Como contribuir com tradução?

Existem algumas formas de contribuir com tradução para o Debian, nesse blog veremos como realizar a tradução das descrições de pacotes (_Debian Description Translation Project_ -- DDTP).

Durante muito tempo a contribuição era realizada por email, porém atualmente, para facilitar a tradução desses pacotes, Martjin van Oosterhout desenvolveu o DDTSS (*Debian Distributed Translation Server Satellite* ou Satélite do Servidor de Traduções Distribuídas Debian), uma interface Web capaz de interagir com o DDTP de uma forma muito simples. 

### Entrando no DDTSS

O DDTSS é um sistema muito fácil de usar. Basta fazer o login, escolher um pacote e fazer a tradução ou a revisão nos campos apropriados. Clique em aceitar as alteração e pronto. Muito simples! 

O primero passo para realizar a contribuição é criar um login no DDTSS no seguinte [link](https://ddtp.debian.org/ddtss/index.cgi/createlogin). O DDTSS atualmente não tem a funcionalidade de restauração de login e senha, portanto guarde suas credenciais com muito cuidado.

Após uma tradução inicial são necessárias três revisões **sem alterações** para que a tradução seja enviada ao banco de dados e posteriormente incluída nos repositórios oficiais. Caso ocorra uma revisão com alteração a contagem é zerada e mais três revisões são necessárias. 

Então é possível tanto contribuir traduzindo pacotes novos quanto revisando pacotes já traduzidos inicialmente. 

### Tradução e revisão

Após realizar o login, basta acessar o DDTSS e ver os pacotes disponíveis para
realizar a tradução, para isso acesse o
[link](https://ddtp.debian.org/ddtss/index.cgi/pt_BR).

Você chegará na seguinte página:
![A página tem um título principal "DDTSS for pt_BR, com várias seções principais, incluindo "Project-Messages", "Pending translation" e "About you"](/imgs/posts/calourada-lkcamp-2023/DDTSS.png) 

Se for sua primeira contribuição, é interessante começar realizando uma revisão, pois é a maneira mais fácil de se acostumar com os padrões do sistema e com os hábitos da equipe.

#### Revisão

Na seção de `Pending review`, é possível verificar os pacotes disponíveis para revisão, basta escolher um e clicar sobre ele. 

Quando abrir o pacote, irá aparecer a caixa de texto `translate` que terá a tradução feita por alguém e a caixa `comment field` necessária para fazer comentários sobre a tradução. 

![Página com as caixas de texto para realizar revisões, incluindo a descrição
curta e longa.](/imgs/posts/calourada-lkcamp-2023/revisao.png)

Analisando a tradução, você pode fazer as alterações que achar necessárias. Se a tradução estiver correta ao seu critério, basta adicionar um comentário no `comment field` com o seguinte padrão:

> AAAAMMDD: nome-de-usuário: ação. comentário.

Alguns exemplos:

> 20230607: MachadoAssis: tradução.
>
> 20120608: MonteiroLobato: revisão. Sugiro usarmos "extensão" para a tradução de "plugin".
>
> 20050609: CeciliaMeireles: revisão. Sugestão aceita.
>
> 20220609: MachadoAssis: revisão.


Após realizar o comentário, clique em `Accept as is` caso não tenha feito alguma alteração na tradução. Caso contrário, clique em `Accept with changes`.

Pronto, sua primeira contribuição foi feita!

#### Tradução 

Para realizar a tradução, entre no [link](https://ddtp.debian.org/ddtss/index.cgi/pt_BR), verifique os pacotes da seção `Pending translation` e selecione um pacote.
 
![Página igual a de revisão, porém desta vez sem preenchimento das caixas de texto.](/imgs/posts/calourada-lkcamp-2023/traducao.png)

Ao abrir, basta traduzir os textos definidos em *Untranslated* nas caixas de
texto *Translated*. É importante levar em consideração alguns tópicos para realizar a tradução nas descrições curtas e longas. 

**Descrição curta**:

- uma única linha, deve ser breve e sumária;
- conter até 80 caracteres e sem ponto final;
- não incluir nome de pacote, mas pode incluir nome de aplicativo;
- deve começar com letra minúscula, exceto quando nome próprio ou sigla;
- evite o uso de artigos, definidos ou indefinidos - seja direto;

**Descrições longa**:

- descreve o pacote, suas dependências e conflitos;
- pode conter de duas a cinco linhas, ou vários parágrafos;
- deve apresentar sentenças curtas e completas;
- evite tabulações no texto;
- quebras de linha são entendidas como espaços entre palavras;
- parágrafos são separados por uma linha que contém somente um ponto (símbolo: .);
- mantenha a diagramação dos textos e das listas;


{{< alerts info"**Aviso:** Para realizar traduções de forma correta, é imprescindível verificar o vocabulário técnico disponível no seguinte [link](https://ddtp.debian.org/ddtss/index.cgi/pt_BR/wordlist)" >}}

Posteriormente, adicione um comentário no campo `Comment
field` com o seguinte padrão: 

> AAAAMMDD: nome-de-usuário: ação. comentário.

Alguns exemplos:

> 20230607: MachadoAssis: tradução.
>
> 20120608: MonteiroLobato: revisão. Sugiro usarmos "extensão" para a tradução de "plugin".
>
> 20050609: CeciliaMeireles: revisão. Sugestão aceita.
>
> 20220609: MachadoAssis: revisão.


Caso tenha terminado a tradução, clique em `Submit` e pronto! 


{{< alerts error
"**Cuidado:** Sempre que desistir de uma tradução aperte o botão ***Abandon***, senão ela ficará bloqueada para qualquer outra pessoa."
>}}

Esperamos que você tenha aprendido e se divertido muito no nosso evento de
recepção. Caso queira dar continuidade às suas contribuições, venha a um de
nossos encontros semanais:

- segunda-feira: 13h00 - 16h00
- quarta-feira: 19h00 - 21h00

Nos vemos lá! :penguin: :blue_heart:
